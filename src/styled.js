import styled, {css} from 'styled-components';


export const Container = styled.div`
    width: 100%;
    overflow: hidden;
    position: relative;
`

export const Queue = styled.ul`
    width: 40000px;
    position: absolute;
    display:block;
    top: 0;
    left: 0;
    margin: 0;
    padding:0;
    overflow: hidden;
`

export const ImageWrapper = styled.li`
    display: block;
    float: left;
    margin: 0;
    padding: 0;
    list-style: none;
    text-align: center;
    background-color: #000;
`

export const Image = styled.img`
    max-width: 70vw;
    height: auto;
`

export const Button = styled.button`
    position: absolute;
    top: 50%;
    width: 65px;
    padding: 10px 0;
    cursor: pointer;
    display: block;
`

export const PrevButton = styled(Button)`
    left: 0;
`

export const NextButton = styled(Button)`
    right: 0;
`