import React, {Component} from 'react';
import classNames from 'classnames';
import delay from 'delay';

import {Container, Queue,  ImageWrapper, Image, PrevButton, NextButton} from './styled';


class QueueSlider extends Component {
    constructor(props) {
        super(props)
        this.state = {
            loading: true,
            queueTransform: '',
            queueTransitionDuration: 0,
        }

        this.sliderLocalState = {
            playing: false,
            busy: false,
            isMobile: window.orientation > -1
        }
        //local state queue
        this.queueSettings = {
            width: 0,
            height: 0
        }

        this.index = {
            active: this.props.mode === 'horizontal' ? 2 : 0,
            previous: null
        };

        //viewport width and height:
        this.viewport = {
            width: window.innerWidth,
            height: null
        }

        if(this.props.touchEnabled) this.touch = {
            original_position: {left: 0, top: 0},
            start: {x: 0, y: 0},
            end: {x: 0, y: 0}
        }

        //count slide
        this.count = this.props.mode === 'horizontal' ? this.props.images.length + 4 : this.props.images.length
        //slides size
        this.slidesSize = [];

        this.css = {
            vendor_prefix: '',
            transform: '',
            transform_duration: ''
        }
        //loading flags
        this.loadingFlags = this.props.mode === 'horizontal' ? Array(this.props.images.length + 4).fill(0) : Array(this.props.images.length).fill(0)
        this.indexOnLoad = -1;
    }

    // componentWillMount() {
    //     this.setVendorCss()
    // }

    async componentDidUpdate() {
        const active = this.index.active
        const {images, transitionSpeed} = this.props

        //set playing slider state

        this.setPlayingSilderState();

        if(active >= images.length + 2) {
            await delay(transitionSpeed + 1)
            this.slideComplete('first')
        }
        //
        if(active <= 1) {
            await delay(transitionSpeed + 1)
            this.slideComplete('last')
        }

    }

    setPlayingSilderState = async  () => {
        const {transitionSpeed} = this.props
        this.sliderLocalState.playing = true;
        await delay(transitionSpeed + 1);
        this.sliderLocalState.playing = false;
    }

    componentDidMount() {
        const {touchEnabled} = this.props;
        touchEnabled && this.initTouch();
        this.resizeListener();
    }

    resizeListener = () => {
        window.addEventListener("resize", () => {
            this.viewport.width = window.innerWidth;
            this.sliderLocalState.isMobile = window.orientation > -1;
            this.computeQueueSizes();
        });
    }

    initTouch = () => {
        this.slider.addEventListener('touchstart', this.onTouchStart, false);
    }

    setPosition = (value, type, speed) => {
        const {transitionSpeed} = this.props
        speed = type === 'init' ? 0 : speed !== undefined ?  speed : transitionSpeed / 1000;

        if(type === 'reset') speed = 0;

        const transitionDuration = `${speed}s`
        const transform = `translate3d(${value}px, 0, 0)`;
        this.setState({queueTransitionDuration: transitionDuration, queueTransform: transform}, async () => {
            
        })
    }

    getQuePosition = () =>  {
        const {alignMode} = this.props;
        const {width} = this.viewport;
        const {active} = this.index;
        let position;

        switch(alignMode) {
            case 'center':
                position = (width - this.slidesSize[active].w) / -2;
                break;
            default: 
        }

        for ( let i = 0; i < active; i++) {
            position += this.slidesSize[i].w;
        }
        return position;
    }

    setOriginalPositionWhenTouchStart = () => {
        this.touch.original_position.left = -this.getQuePosition()
        this.touch.original_position.top = 0;
    }

    onTouchStart = (e) => {
        this.setOriginalPositionWhenTouchStart();

        this.touch.start.x = e.changedTouches[0].pageX;
        this.touch.start.y = e.changedTouches[0].pageY;

        this.slider.addEventListener('touchmove', this.onTouchMove, false);
        this.slider.addEventListener('touchend', this.onTouchEnd, false);
    }

    onTouchMove = (e) => {
        const {mode} = this.props
        let x_movement = Math.abs(e.changedTouches[0].pageX - this.touch.start.x),
            y_movement = Math.abs(e.changedTouches[0].pageY - this.touch.start.y),
            change = e.changedTouches[0].pageX - this.touch.start.x;

        if ((x_movement * 3) > y_movement) {
            e.preventDefault();
        }

        if (mode === 'horizontal') {
            this.setPosition(this.touch.original_position.left + change, 'reset', 0);
        }
    }

    onTouchEnd = (e) => {
        const {swipeThreshold, mode} = this.props

        this.slider.removeEventListener('touchmove', () => {});
        this.touch.end.x = e.changedTouches[0].pageX;
        this.touch.end.y = e.changedTouches[0].pageY;

        let distance = this.touch.end.x - this.touch.start.x;
        if (Math.abs(distance) >= swipeThreshold) {
            if (distance < 0) {
                this.nextSlide();
            } else {
            this.previousSlide();
        }
        } else if (mode === 'horizontal') {
            this.setPosition(this.touch.original_position.left, 'reset', 0);
        }
        this.slider.removeEventListener('touchend', () => {});
    }


    nextSlide = () => {
        this.slide(1);
    }


    previousSlide = () => {
        this.slide(-1);
    }


    computeQueueSizes = () => {
        const {offScreen, mode} = this.props
        const {width : widthViewPort} = this.viewport
        let previous_slide, queue_width = 0, queue_height = 0

        const slides = [...this.queue.querySelectorAll('li > img')];
        slides.forEach((slide, index) => {

            if(offScreen) {
                const widthSlide = slide.offsetWidth,
                margins = widthSlide - widthViewPort
                this.slidesSize[index] = {
                    w: widthSlide + (margins > 0 ? margins : 0),
                    h: null
                }

                queue_width += this.slidesSize[index].w
            }else {
                this.slidesSize[index] = {
                    w: slide.offsetWidth,
                    h: null,
                }
                queue_width += this.slidesSize[index].w
            }

            //
            if(index === 0) queue_height = slide.offsetHeight;

            if(previous_slide) {
                this.slidesSize[index - 1].h = previous_slide.offsetHeight;
                if(this.slidesSize[index - 1].h < queue_height) queue_height = this.slidesSize[index - 1].h;
            }

            previous_slide = slide;

        })

        // Calculate the last slided's height after adjusting the width.
        if (previous_slide) {
            this.slidesSize[this.count - 1].h = previous_slide.offsetHeight;
            if (this.slidesSize[this.count - 1].h < queue_height) {
                queue_height = this.slidesSize[this.count - 1].h
            }
        }

        if(mode === 'horizontal') {
            this.queueSettings.width = queue_width;
            this.queueSettings.height = queue_height;
            this.setPosition(-this.getQuePosition(), 'reset')
        } else {
            this.queueSettings.width = "100%";
            this.queueSettings.height = queue_height;
            this.setPosition(-this.getQuePosition(), 'reset')
        }

    } 

    handleImageLoaded = () => {
        this.indexOnLoad +=1;
        this.loadingFlags[this.indexOnLoad] = 1;
        //check whether all images was loaded
        if(this.loadingFlags.every(l => l === 1)) {
            this.computeQueueSizes()
            this.setState({loading: false})
        }
    }

    renderImages = () => {
        const {images, mode} = this.props
        let newImages = [...images];
        if(mode === 'horizontal') {
            newImages.push(images[0]);
            newImages.push(images[1]);
            newImages.unshift(images[images.length - 1]);
            newImages.unshift(images[images.length - 2])
            const length = newImages.length;
            newImages = newImages.map((url, index) => {
                if(index === 0 || index === 1 || index === length - 1 || index === length - 2 ) {
                    return {
                        url: url,
                        type: 'clone'
                    }
                }

                return {
                    url: url,
                    type: 'original'
                }
            })
        }
        let {active} = this.index

        return newImages.map((obj, index) => {
            let imageWClass = classNames({
                'clone': obj.type === 'clone',
                'active': active === index
            })

            return (
                <ImageWrapper key={index} className={imageWClass}>
                    <Image src={obj.url} onLoad={this.handleImageLoaded} style={{opacity: active === index ? 1 : 0.3}} />
                </ImageWrapper>
            )
        })
    }

    setVendorCss = () => {
        const div = document.createElement("div"),
            props = ['WebkitPerspective', 'MozPerspective', 'OPerspective', 'msPerspective'];

          // Test for each property.
        for (var i in props) {
            if (div.style[props[i]] !== undefined) {
                this.css.vendor_prefix = '-' + props[i].replace('Perspective', '').toLowerCase();
                this.css.transform = this.css.vendor_prefix + '-transform';
                this.css.transform_duration = this.css.vendor_prefix + '-transition-duration';
            }
        }
    }

    slide = (index) => {
        const {playing} = this.sliderLocalState;
        this.index.active +=index;
        this.setPosition(-this.getQuePosition(), 'slide');
        if(!playing) {
        }
    }

    slideComplete = (resetType) => {
        const {mode, images} = this.props;

        if(mode === 'horizontal') {
            this.index.active = resetType === 'first' ? 2 : images.length + 1;
            this.setPosition(-this.getQuePosition(), 'reset');
        }
    }

    render() {
        const {previous, next} = this.props;
        const {loading, queueTransitionDuration, queueTransform} = this.state
        const {width, height} = this.queueSettings

        //apply style to Queue
        const styleQueue = !loading ? {
            width: width, 
            height: height,
            transitionDuration: queueTransitionDuration,
            transform: queueTransform
        } : {}
        return (
            <Container className="queueslider" ref={element => (this.slider = element)} style={{height: height}}>
                <Queue className="queue" ref={element => (this.queue = element)} style={styleQueue}>
                    {this.renderImages()}
                </Queue>
                <PrevButton onClick={() => this.slide(-1)} >{previous}</PrevButton>
                <NextButton onClick={() => this.slide(1)}>{next}</NextButton>
            </Container>
        )
    }
}

export default QueueSlider;