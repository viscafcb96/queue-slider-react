import React from 'react';
import logo from './logo.svg';
import QueueSlider from './QueueSlider';
import Gallery from './Gallery';
import './App.css';

const images =[ "http://thuthuatphanmem.vn/uploads/2018/05/18/hinh-nen-may-tinh-hd-bai-bien-trong-xanh-ngay-nang-dep_024420660.jpg",
            "http://thuthuatphanmem.vn/uploads/2018/05/18/hinh-nen-may-tinh-hd-cau-noi-tieng-anh-hay-va-y-nghia-neu-ba_cjzz9_024421269.jpg",
            "http://thuthuatphanmem.vn/uploads/2018/05/18/hinh-nen-may-tinh-hd-cau-noi-tieng-anh-hay-lam-nhieu-hon-nhu_xtCpq_024421237.jpg",
            "http://thuthuatphanmem.vn/uploads/2018/05/18/hinh-nen-may-tinh-hd-cay-au-tren-mat-nuoc-dep_024421425.jpg",
            "http://thuthuatphanmem.vn/uploads/2018/05/18/hinh-nen-may-tinh-hd-chao-buoi-sang-voi-mot-coc-tra-de-thuon_kybZy_024421471.jpg",
            "http://thuthuatphanmem.vn/uploads/2018/05/18/hinh-nen-may-tinh-hd-co-gai-deo-headphone-va-dan-ghita-dep_024421534.jpg",
            "http://thuthuatphanmem.vn/uploads/2018/05/18/hinh-nen-may-tinh-hd-coc-doi-tinh-yeu-dep_024421596.jpg",
            "http://thuthuatphanmem.vn/uploads/2018/05/18/hinh-nen-may-tinh-hd-good-morning-dep_024422049.jpg",
            "http://thuthuatphanmem.vn/uploads/2018/05/18/hinh-nen-may-tinh-hd-la-phong-xanh-dep_024422517.jpg",
            "http://thuthuatphanmem.vn/uploads/2018/05/18/hinh-nen-may-tinh-hd-nhu-mot-bong-cuc-cuc-dep_024422579.jpg"

        ]

function App() {
  return (
    <div className="App">
      {/* <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.js</code> and save to reload.
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
      </header> */}
      <QueueSlider 
        images={images} 
        mode='horizontal'
        previous="Previous"
        next="next"
        alignMode="center"
        transitionSpeed ={700}
        offScreen={false}
        touchEnabled={true}
        swipeThreshold={50}
      />
      {/* <Gallery imageUrls={images} /> */}
    </div>
  );
}

export default App;
